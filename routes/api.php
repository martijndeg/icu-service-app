<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    Route::get('/users', 'UserController@index');
    Route::get('/users/{user}', 'UserController@show');
    Route::put('/users/{user}', 'UserController@update');
    Route::delete('/users/{user}', 'UserController@destroy');
    Route::post('/users', 'UserController@store');

    Route::get('/customers/all', 'CustomerController@indexAll');
    Route::get('/customers', 'CustomerController@index');
    Route::post('/customers', 'CustomerController@store');
    Route::get('/customers/{customer}', 'CustomerController@show');
    Route::put('/customers/{customer}', 'CustomerController@update');
    Route::delete('/customers/{customer}', 'CustomerController@destroy');

    Route::get('/orders/all', 'OrderController@indexAll');
    Route::get('/orders', 'OrderController@index');
    Route::post('/orders', 'OrderController@store');
    Route::get('/orders/{order}', 'OrderController@show');
    Route::put('/orders/{order}', 'OrderController@update');

    Route::post('/invoices', 'InvoiceController@store');
    Route::get('/invoices/{order}/file', 'InvoiceController@getFile');
});
