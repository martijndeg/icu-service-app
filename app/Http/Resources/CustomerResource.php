<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'company' => $this->company,
            'contact' => $this->contact,
            'email'   => $this->email,
            'phone'   => $this->phone,
            'street'  => $this->street,
            'houseno' => $this->houseno,
            'zipcode' => $this->zipcode,
            'city'    => $this->city,
            'country' => $this->country,
            'rate'    => $this->rate,
        ];
    }
}
