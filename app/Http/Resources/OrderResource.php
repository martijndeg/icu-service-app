<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $orderLines = [];
        $total = 0;

        foreach($this->lines as $line) {
            $orderLines[] = [
                'id'          => $line->id,
                'amount'      => $line->amount,
                'description' => $line->description,
                'price'       => $line->price,
                'date'        => $line->created_at,
            ];

            $total += $line->amount * $line->price;
        }

        return [
            'id'          => $this->id,
            'orderNumber' => $this->getOrderNumber(),
            'customer'    => $this->customer,
            'customer_id' => $this->customer_id,
            'description' => $this->description,
            'date'        => $this->created_at->format('d-m-Y H:i:s'),
            'status'      => $this->status,
            'lines'       => $orderLines,
            'total'       => $total,
        ];
    }
}
