<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\InvoiceResource;
use App\Http\Resources\OrderResource;
use App\Order;
use App\Invoice;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as Pdf;
use Carbon\Carbon;

class InvoiceController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->validate(Invoice::$validationRules);

        $order = Order::find($data['order_id']);

        $invoiceData = [];

        $orderResource = new OrderResource(Order::find($data['order_id']));

        $invoiceData['order'] = $orderResource->toArray($request);
        $invoiceData['date'] = $data['date'];

        $pdf = Pdf::loadView('pdf.invoice', $invoiceData);

        $fileName = Carbon::now()->format('YmdHis') . rand(1000000, 9999999) . '.pdf';
        $path = storage_path('invoices/') . $fileName;

        if($pdf->save($path)) {
            $invoice = $order->invoices()->save(Invoice::create([
                'date' => Carbon::now(),
                'reference' => 'reference',
            ]));

            $order->status = 1;

            $order->save();

            return new InvoiceResource($invoice);
        }

    }

    public function getFile(int $id)
    {
        $invoice = Invoice::find($id);

        return response()->json(['url' => storage_path('invoices/') . $invoice->file]);
    }

    // public function generate(Request $request)
    // {
    //     $data = $request->validate(Invoice::$validationRules);



    //     // $pdf = Pdf::loadView('pdf.invoice', $data, [], [
    //     //     'format' => 'A4'
    //     // ])->save(storage_path('test-invoice/') . 'test.pdf');

    //     // return response()->json($data['order_id']);
    //     // return $pdf->stream();
    //     // return $pdf->save(storage_path('test-invoice/') . 'test.pdf');
    // }
}
