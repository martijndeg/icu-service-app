<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderLine;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        return OrderResource::collection(Order::orderByDesc('id')->paginate(10));
    }

    public function indexAll()
    {
        return OrderResource::collection(Order::orderBy('description')->get());
    }

    public function show(Order $order)
    {
        return new OrderResource($order);
    }

    public function store(Request $request)
    {
        $data = $request->validate(Order::$validationRules, Order::$validationMessages);

        $order = Order::create([
            'customer_id' => $data['customer_id'],
            'description' => $data['description'],
        ]);

        foreach($data['orderlines'] as $line) {
            $order->lines()->save(OrderLine::create([
                'amount'      => $line['amount'],
                'description' => $line['description'],
                'price'       => $line['price'],
            ]));
        }

        return new OrderResource($order);
    }

    public function update(Order $order, Request $request)
    {
        $data = $request->validate(Order::$validationRules, Order::$validationMessages);

        $order->update($data);

        $lines = $data['lines'];

        $deletedLines = [];

        for($i = 0; $i < sizeof($lines); $i++) {
            $order->lines()->updateOrCreate([
                'amount'      => $lines[$i]['amount'],
                'description' => $lines[$i]['description'],
                'price'       => $lines[$i]['price'],
            ]);
        }

        $order->lines()->where('amount', 0)->orWhere('price', 0)->delete();

        return new OrderResource($order);
    }

}
