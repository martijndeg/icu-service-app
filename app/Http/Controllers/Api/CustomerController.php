<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use App\Http\Resources\CustomerResource;

class CustomerController extends Controller
{
    public function index()
    {
        return CustomerResource::collection(Customer::orderBy('company')->paginate(10));
    }

    public function indexAll()
    {
        return CustomerResource::collection(Customer::orderBy('company')->get());
    }

    public function show(Customer $customer)
    {
        return new CustomerResource($customer);
    }

    public function store(Request $request)
    {
        $data = $request->validate(Customer::$validationRules, Customer::$validationMessages);

        return new CustomerResource(Customer::create([
            'company' => $data['company'],
            'contact' => $data['contact'],
            'email'   => $data['email'],
            'phone'   => $data['phone'],
            'street'  => $data['street'],
            'houseno' => $data['houseno'],
            'zipcode' => $data['zipcode'],
            'city'    => $data['city'],
            'country' => $data['country'],
            'rate'    => (int) $data['rate'] * 100,
            'notes'   => $data['notes'],
        ]));
    }

    public function update(Customer $customer, Request $request)
    {
        $data = $request->validate(Customer::$validationRules, Customer::$validationMessages);

        $customer->update($data);

        return new CustomerResource($customer);
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();

        return response(null, 204);
    }
}
