<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public static $validationRules = [
        'order_id' => ['required', 'min:1'],
        'date'     => ['required', 'date'],
    ];

    protected $fillable = ['date', 'order_id', 'status'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
