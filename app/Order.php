<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['id', 'customer_id', 'description', 'status'];

    public static $validationRules = [
        'description'         => ['required', 'string', 'min:3'],
        'customer_id'         => ['required', 'numeric', 'min:1'],
        'status'              => ['nullable', 'numeric'],
        'lines.*.amount'      => ['required', 'numeric'],
        'lines.*.description' => ['required', 'string'],
        'lines.*.price'       => ['required', 'numeric'],
    ];

    public static $validationMessages = [
        'customer_id.required' => 'Please select a customer.',
        'customer_id.numeric'  => 'Please select a customer.',
    ];

    public function getOrderNumber()
    {
        return 'ICU-O' . now()->format('y') . '-' . str_pad($this->id, 6, '0', STR_PAD_LEFT);
    }

    public function lines()
    {
        return $this->hasMany(OrderLine::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
}
