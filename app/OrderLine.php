<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model
{
    protected $fillable = ['amount', 'description', 'price'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
