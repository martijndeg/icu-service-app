<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['company', 'contact', 'email', 'phone', 'street', 'houseno', 'zipcode', 'city', 'country', 'rate', 'notes'];

    public static $validationRules = [
        'company' => ['required', 'string'],
        'contact' => ['required', 'string'],
        'email'   => ['required', 'email'],
        'phone'   => ['required', 'string', 'min:10'],
        'street'  => ['required', 'string'],
        'houseno' => ['required', 'string'],
        'zipcode' => ['required', 'string'],
        'city'    => ['required', 'string'],
        'country' => ['required', 'string'],
        'rate'    => ['required', 'numeric'],
        'notes'   => ['nullable', 'string'],
    ];

    public static $validationMessages = [
        'houseno.required' => 'The house number field is required.',
        'houseno.string'   => 'The house number fiels has to be a string.',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

}
