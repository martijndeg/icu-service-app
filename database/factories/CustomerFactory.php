<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'company' => $faker->company,
        'contact' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'street' => $faker->streetName,
        'houseno' => rand(1, 45),
        'city' => $faker->city,
        'zipcode' => $faker->postcode,
        'country' => $faker->country,
        'rate' => 5500,
        'notes' => $faker->text(45),
    ];
});
