import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

require('particles.js');

import App from './views/App'
import Hello from './views/Hello'
import Home from './views/Home'
import UsersIndex from "./views/UsersIndex";
import UsersEdit from './views/UsersEdit';
import CustomerIndex from './views/CustomerIndex';
import CustomerCreate from './views/CustomerCreate';
import CustomerEdit from './views/CustomerEdit';
import OrderIndex from './views/OrderIndex';
import OrderCreate from './views/OrderCreate';
import OrderEdit from './views/OrderEdit';
import InvoiceIndex from './views/InvoiceIndex';
import InvoiceCreate from './views/InvoiceCreate';
import NotFound from './views/NotFound';
import UsersCreate from './views/UsersCreate';

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/admin',
            name: 'home',
            component: Home
        },
        {
            path: '/admin/hello',
            name: 'hello',
            component: Hello,
        },
        {
            path: '/admin/users',
            name: 'users.index',
            component: UsersIndex,
        },
        {
            path: '/admin/users/:id/edit',
            name: 'users.edit',
            component: UsersEdit,
        },
        {
            path: '/admin/users/create',
            name: 'users.create',
            component: UsersCreate,
        },
        {
            path: '/admin/customers',
            name: 'customers.index',
            component: CustomerIndex,
        },
        {
            path: '/admin/customers/create',
            name: 'customers.create',
            component: CustomerCreate,
        },
        {
            path: '/admin/customers/:id/edit',
            name: 'customers.edit',
            component: CustomerEdit,
        },
        {
            path: '/admin/orders',
            name: 'orders.index',
            component: OrderIndex,
        },
        {
            path: '/admin/orders/create',
            name: 'orders.create',
            component: OrderCreate,
        },
        {
            path: '/admin/orders/:id/edit',
            name: 'orders.edit',
            component: OrderEdit,
        },
        {
            path: '/admin/invoices',
            name: 'invoices.index',
            component: InvoiceIndex,
        },
        {
            path: '/admin/invoices/create',
            name: 'invoices.create',
            component: InvoiceCreate,
        },
        {
            path: '/admin/404',
            name: '404',
            component: NotFound
        },
        {
            path: '/admin/*',
            redirect: '/admin/404'
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});

Vue.use(VueSweetalert2);

if(document.getElementById('particles-js')) {
    particlesJS.load('particles-js', '/assets/particles.json', function() {
        console.log('callback - particles.js config loaded');
    });
}
