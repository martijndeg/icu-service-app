
var currentSec = getSecondsToday();

var seconds = (currentSec / 60) % 1;
var minutes = (currentSec / 3600) % 1;
var hours = (currentSec / 43200) % 1;

setTime(60 * seconds, "second");
setTime(3600 * minutes, "minute");
setTime(43200 * hours, "hour");
showDate();

function setTime(left, hand) {
    let hands = document.querySelectorAll(".clock__" + hand);
    for(let i = 0; i < hands.length; i++) {
        hands[i].style.animationDelay = left * -1 + "s";
    }
}

function getSecondsToday() {
    let now = new Date();

    let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    let diff = now - today;
    return Math.round(diff / 1000);
}

function showDate() {
    let now = new Date();

    let weekday = dayName(now.getDay());
    let day = now.getDate();
    let month = monthName(now.getMonth());
    let year = now.getFullYear();
    
    if(document.getElementById('weekday') !== null) {
        document.getElementById('weekday').innerText = weekday;
        document.getElementById('day').innerText = day.toString();
        document.getElementById('month').innerText = month.toString();
        document.getElementById('year').innerText = year.toString();
    }
}

function dayName(day) {
    let weekdays = new Array(7);

    weekdays[0] = "Sun";
    weekdays[1] = "Mon";
    weekdays[2] = "Tue";
    weekdays[3] = "Wed";
    weekdays[4] = "Thu";
    weekdays[5] = "Fri";
    weekdays[6] = "Sat";

    return weekdays[day];
}

function monthName(month) {
    let months = new Array(12);

    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    return months[month];
}
