import axios from 'axios';

export default {
    all() {
        return axios.get('/api/invoices');
    },
    find(id) {
        return axios.get(`/api/invoices/${id}`);
    },
    update(id, data) {
        return axios.put(`/api/invoices/${id}`, data);
    },
    delete(id) {
        return axios.delete(`/api/invoices/${id}`);
    },
    create(data) {
        return axios.post('/api/invoices', data);
    },
    getFile(id) {
        return axios.get(`/api/invoices/${id}/file`, data);
    }
}
