import axios from 'axios';

export default {
    all() {
        return axios.get('/api/orders');
    },
    find(id) {
        return axios.get(`/api/orders/${id}`);
    },
    update(id, data) {
        return axios.put(`/api/orders/${id}`, data);
    },
    delete(id) {
        return axios.delete(`/api/orders/${id}`);
    },
    create(data) {
        return axios.post('/api/orders', data);
    },
}
