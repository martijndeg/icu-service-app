@extends('layouts.app')

@section('content')
<section class="hero is-fullheight login-form">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-white">Let's reset your password</h3>
                <hr class="login-hr">
                <p class="subtitle has-text-white">Please enter your email to proceed.</p>
                <div class="box">
                    <figure class="avatar">
                        <img src="{{ asset('images/cap-obvious.jpg') }}">
                    </figure>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        @if (session('status'))
                            <div class="notification is-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="field">
                            <div class="control @error('email') has-icons-right @enderror">
                                <input id="email" type="email" class="input is-large @error('email') is-danger @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Your Email" >
                                @error('email')
                                    <span class="icon is-small is-danger is-right">
                                        <i class="icofont-warning"></i>
                                    </span>
                                @enderror
                            </div>
                            @error('email')
                                <p class="help is-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <button type="submit" class="button is-block is-info is-large is-fullwidth">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </form>
                </div>
                <p class="has-text-grey">
                    <a class="btn btn-link" href="{{ route('login') }}">
                        {{ __('Now you remember?') }}
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>


{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
