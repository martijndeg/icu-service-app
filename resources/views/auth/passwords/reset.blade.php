@extends('layouts.app')

@section('content')
<section class="hero is-fullheight login-form">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-white">New Password</h3>
                <hr class="login-hr">
                <p class="subtitle has-text-white">Please choose a new Password to proceed.</p>
                <div class="box">
                    <figure class="avatar">
                        <img src="{{ asset('images/cap-obvious.jpg') }}">
                    </figure>
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <div class="field">
                            <div class="control @error('email') has-icons-right @enderror">
                                <input id="email" type="email" class="input is-large @error('email') is-danger @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Your Email" >
                                @error('email')
                                    <span class="icon is-small is-danger is-right">
                                        <i class="icofont-warning"></i>
                                    </span>
                                @enderror
                            </div>
                            @error('email')
                                <p class="help is-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="field">
                            <div class="control @error('password') has-icons-right @enderror">
                                <input id="password" type="password" class="input is-large @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Your new Password">
                                @error('password')
                                    <span class="icon is-small is-danger is-right">
                                        <i class="icofont-warning"></i>
                                    </span>
                                @enderror
                            </div>
                            @error('password')
                                <p class="help is-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="field">
                            <div class="control @error('password_confirmation') has-icons-right @enderror">
                                <input id="password-confirm" type="password" class="input is-large @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm your new Password">
                                @error('password_confirmation')
                                    <span class="icon is-small is-danger is-right">
                                        <i class="icofont-warning"></i>
                                    </span>
                                @enderror
                            </div>
                            @error('password_confirmation')
                                <p class="help is-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <button type="submit" class="button is-block is-info is-large is-fullwidth">
                            {{ __('Reset Password') }}
                        </button>
                    </form>
                </div>
                <p class="has-text-grey">
                    <a class="btn btn-link" href="{{ route('login') }}">
                        {{ __('Now you remember?') }}
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>






{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
