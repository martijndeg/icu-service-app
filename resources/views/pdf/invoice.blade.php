<html>
    <head>
        <title>Hello!</title>
        <style>
            body {
                font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
            }

            #meta,
            #order_lines {
                width: 100%;
            }

            #meta {
                margin-bottom: 40px;
            }

            #meta .address {
                text-align: right;
            }

            #meta .address > table {
                width: 100%;
            }

            #meta .address td {
                text-align: right;
            }

            #order_lines td,
            #order_lines th {
                border-left: 0;
                border-right: 0;
            }

            #order_lines tr:nth-of-type(odd) {
                background: #616161;
                color: #fff;
            }

            #order_lines tr:nth-of-type(even) {
                background: #fff;
                color: #616161;
            }

            #order_lines tr:last-of-type {
                border-top: #616161 solid 1px;
                background: #fff;
                color: #616161;
            }

            #order_lines .price,
            #order_lines .subtotal,
            #order_lines .total {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <table id="meta">
            <tr>
                <td></td>
                <td class="address">
                    <table cellspacing="0">
                        <tr>
                            <td>{{ $order['customer']->company }}</td>
                        </tr>
                        <tr>
                            <td>{{ $order['customer']->contact }}</td>
                        </tr>
                        <tr>
                            <td>{{ $order['customer']->street }} {{ $order['customer']->houseno }}</td>
                        </tr>
                        <tr>
                            <td>{{ strtoupper($order['customer']->zipcode) }} {{ strtoupper($order['customer']->city) }} </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>Ordernummer: {{ $order['orderNumber'] }}</td>
            </tr>
            <tr>
                <td>Factuurdatum: {{ Carbon\Carbon::parse($order['date'])->format('d-m-Y') }}</td>
            </tr>
            <tr>
                <td>Factuurnummer:</td>
            </tr>
        </table>
        <table id="order_lines">
            @foreach($order['lines'] as $line)
            <tr>
                <td>{{ $line['amount'] }}</td>
                <td class="description">{{ $line['description'] }}</td>
                <td class="price">&euro; {{ number_format($line['price'] / 100, 2, ',', '.') }}</td>
                <th class="subtotal">&euro; {{ number_format(($line['price'] * $line['amount']) / 100, 2, ',', '.') }}</th>
            </tr>
            @endforeach
            <tr>
                <td colspan="2"></td>
                <td class="total">Total:</td>
                <td class="total">&euro; {{ number_format($order['total'] / 100, 2, ',', '.') }}</td>
            </tr>
        </table>

        {{-- <pre>
            {{ var_dump($order) }}
        </pre> --}}
    </body>
</html>






